﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace GamingForest.Owl.Server
{
    public class ServerNetworkService
    {
        public string EndPointString = "XXX.XXX.XXX.XXX:XXXX/XXXX";

        public ServerConnectionStatus Status
        { 
            get 
            { 
                return _status; 
            }
            private set 
            { 
                _status = value;
                OnConnectionStatusChanged.Invoke(this, value);
            }
        }
        public enum ServerConnectionStatus
        {
            Shutdown = 0,
            AwaitingConnection = 1,
            ClientConnected = 2,
            ClientDisconnected = 3,
            Error = 4
        }

        public EventHandler<ServerConnectionStatus> OnConnectionStatusChanged;

        private ServerConnectionStatus _status = ServerConnectionStatus.Shutdown;

        private bool _running = false;

        private const int MSG_BUFFER_SIZE = 1024;

        private Thread _serverThread;

        private Socket _messageListener;
        private Socket _audioListener;

        private Socket _messageClient;
        private Socket _audioClient;

        private byte[] _audioSendBuffer;

        private bool _handshake;

        private readonly MainActivity _context;

        public ServerNetworkService(MainActivity context)
        {
            _context = context;
        }


        public void StartServer()
        {
            if (_serverThread != null)
            {
                StopServer();
                _serverThread?.Join();
            }

            if (_serverThread != null)
                throw new Exception("Could not stop existing server thread!");

            _serverThread = new Thread(ServerRoutine) 
            { 
                Name = nameof(ServerRoutine)
            };
            _serverThread.Start();
        }

        private void ServerRoutine()
        {
            try
            {
                _context.LogOnScreen("Started Server Routine...");

                _running = true;

                IPHostEntry ipHost = Dns.GetHostEntry(Dns.GetHostName());
                IPAddress serverIp = ipHost.AddressList[0];
                
                IPEndPoint msgEndPoint = new IPEndPoint(serverIp, SettingsActivity.BroadcastMessagePort);
                IPEndPoint audioEndPoint = new IPEndPoint(serverIp, SettingsActivity.BroadcastAudioPort);

                _messageListener = new Socket(serverIp.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                _audioListener = new Socket(serverIp.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                EndPointString = $"{serverIp}:{SettingsActivity.BroadcastMessagePort}/{SettingsActivity.BroadcastAudioPort}";

                _messageListener.Bind(msgEndPoint);
                _audioListener.Bind(audioEndPoint);

                _messageListener.Listen(1);
                _audioListener.Listen(1);

                Thread serverMessageThread = new Thread(ReceiveMessageThread) 
                { 
                    Name = nameof(ReceiveMessageThread)
                };
                Thread serverAudioThread = new Thread(SendAudioThread)
                {
                    Name = nameof(SendAudioThread)
                };

                serverMessageThread.Start();
                serverAudioThread.Start();
                
                while (_running)
                {
                    _handshake = false;

                    Status = ServerConnectionStatus.AwaitingConnection;

                    _messageClient = _messageListener.Accept();
                    _context.LogOnScreen($"[MESSAGE] Connected to {_messageClient.RemoteEndPoint}...");
                    
                    _audioClient = _audioListener.Accept();
                    _context.LogOnScreen($"[AUDIO] Connected to {_audioClient.RemoteEndPoint}...");

                    Status = ServerConnectionStatus.ClientConnected;

                    _context.LogOnScreen("Waiting for client handshake...");

                    while (!_handshake)
                        Thread.Sleep(100);

                    _context.LogOnScreen("Client handshake completed...");

                    while (_running && _messageClient != null && _audioClient != null && _messageClient.Connected && _audioClient.Connected)
                        Thread.Sleep(100);

                    _context.LogOnScreen("Disconnecting from Clients...");

                    TryDisposeClients();
                    
                    Status = ServerConnectionStatus.ClientDisconnected;
                }
            }
            catch (Exception e)
            {
                if (e is SocketException se)
                    _context.LogOnScreen($"{se.SocketErrorCode} | {se.Message}");
                else
                    _context.LogOnScreen(e.Message);

                Status = ServerConnectionStatus.Error;
            }
            finally
            {
                _running = false;

                TryDisposeClients();
                TryDisposeListeners();

                Status = ServerConnectionStatus.Shutdown;

                _serverThread = null;
                
                _context.LogOnScreen("Ended Server Routine...");
            }
        }

        public void StopServer()
        {
            SendMessageToClient("<R2D> Server shutting down...");

            _running = false;

            Thread.Sleep(500);

            TryDisposeListeners();
        }


        private void TryDisposeClients()
        {
            try
            {
                if (_messageClient != null)
                {
                    if (_messageClient.Connected)
                        _messageClient.Shutdown(SocketShutdown.Both);

                    _messageClient.Close();
                    _messageClient.Dispose();
                }

                if (_audioClient != null)
                {
                    if (_audioClient.Connected)
                        _audioClient.Shutdown(SocketShutdown.Both);

                    _audioClient.Close();
                    _audioClient.Dispose();
                }
            }
            catch (Exception e)
            {
                _context.LogOnScreen(e);
            }
        }
        private void TryDisposeListeners()
        {
            try
            {
                if (_messageListener != null)
                {
                    _messageListener.Close();
                    _messageListener.Dispose();
                }

                if (_audioListener != null)
                {
                    _audioListener.Close();
                    _audioListener.Dispose();
                }
            }
            catch (Exception e)
            {
                _context.LogOnScreen(e);
            }
        }


        private void ReceiveMessageThread()
        {
            byte[] receiveBuffer = new byte[MSG_BUFFER_SIZE];
            string msgReceived;
            int byteCount;

            while (_running)
            {
                try
                {
                    if (_messageClient == null || _messageClient.Connected == false)
                        continue;

                    byteCount = _messageClient.Receive(receiveBuffer);
                    msgReceived = Encoding.ASCII.GetString(receiveBuffer, 0, byteCount);

                    _context.LogOnScreen($"IN from {_messageClient.RemoteEndPoint}: {msgReceived}");

                    if (msgReceived.StartsWith("<R2D>"))
                        TryDisposeClients();
                    else if (msgReceived.StartsWith("<R2C>"))
                    {
                        SendMessageToClient($"<R2C> I'M NOT AN OWL!!!\n<BUFFER>{MainActivity.AudioInputBufferSize}\n<RATE>{MainActivity.AudioInputSampleRate}\n<CHANNELS>{(MainActivity.ChannelInputType == Android.Media.ChannelIn.Stereo ? 2 : 1)}\n<ENCODING>{MainActivity.AudioInputEncoding}");

                        Thread.Sleep(100);

                        _handshake = true;
                    }
                }
                catch (Exception e)
                {
                    if (e is SocketException se)
                        _context.LogOnScreen($"{se.SocketErrorCode} | {se.Message}");
                    else
                        _context.LogOnScreen(e);
                }
            }
        }

        private void SendAudioThread()
        {
            int byteCount;
            _audioSendBuffer = new byte[MainActivity.AudioInputBufferSize];

            while (_running)
            {
                try
                {
                    if (_context.Recorder == null)
                        continue;


                    byteCount = _context.Recorder.Read(_audioSendBuffer, 0, _audioSendBuffer.Length);


                    if (_context.Muted)
                        Array.Clear(_audioSendBuffer, 0, byteCount);


                    if (_audioClient == null || !_audioClient.Connected || !_handshake)
                        continue;


                    _audioClient.Send(_audioSendBuffer, byteCount, SocketFlags.None);
                }
                catch (Exception e)
                {
                    if (e is SocketException se)
                        _context.LogOnScreen($"{se.SocketErrorCode} | {se.Message}");
                    else
                        _context.LogOnScreen(e);
                }
            }
        }
        
        public short[] SampleSendData(int sampleSize)
        {
            short[] sample = new short[sampleSize];

            if (_audioSendBuffer != null)
                Buffer.BlockCopy(_audioSendBuffer, 0, sample, 0, sampleSize);

            return sample;
        }


        private void SendMessageToClient(string msg)
        {
            try
            {
                if (_messageClient == null || _messageClient.Connected == false)
                    return;

                _context.LogOnScreen($"OUT to {_messageClient.RemoteEndPoint}: {msg}");

                _messageClient.Send(Encoding.ASCII.GetBytes(msg));
            }
            catch (Exception e)
            {
                _context.LogOnScreen(e);
            }
        }

    }
}