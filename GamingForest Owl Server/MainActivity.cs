﻿using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;

namespace GamingForest.Owl.Server
{   
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true, ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : AppCompatActivity
    {
        private View _mainContent;
        public View MainContent 
        { 
            get 
            {
                if (_mainContent == null)
                    _mainContent = FindViewById<View>(Resource.Id.mainContent);

                return _mainContent;
            }
        }

        private TextView _audioVolumeView;
        public TextView AudioVolumeView
        {
            get
            {
                if (_audioVolumeView == null)
                    _audioVolumeView = FindViewById<TextView>(Resource.Id.audioVolumeBar);
                return _audioVolumeView;
            }
        }

        private TextView _broadcasterIPLabel;
        public TextView BroadcasterIPLabel
        {
            get
            {
                if (_broadcasterIPLabel == null)
                    _broadcasterIPLabel = FindViewById<TextView>(Resource.Id.broadcasterIPLabel);

                return _broadcasterIPLabel;
            }
        }

        private TextView _consoleOutLabel;
        public TextView ConsoleOutLabel
        {
            get
            {
                if (_consoleOutLabel == null)
                    _consoleOutLabel = FindViewById<TextView>(Resource.Id.consoleOutputLabel);

                return _consoleOutLabel;
            }
        }


        private static bool _broadcasting = false;
        public static bool UpdateAudioLoop { get { return _broadcasting; } }


        private Thread _audioVisualizationLoopThread;

        private ServerNetworkService _networkService;


        // Audio Stream Settings
        private AudioRecord _recorder = null;
        public AudioRecord Recorder { get { return _recorder; } }

        private bool _muted = false;
        public bool Muted { get { return _muted; } }

        public static AudioSource AudioInputSource = AudioSource.Default;
        public static int AudioInputSampleRate = 44100;
        public static ChannelIn ChannelInputType = ChannelIn.Default;
        public static Android.Media.Encoding AudioInputEncoding = Android.Media.Encoding.Pcm16bit;
        public static int AudioInputBufferSize = 1024;
        
        private static (int samplerate, int minbuffersize)[] _supportedSamplerates;
        public static (int samplerate, int minbuffersize)[] SupportedSamplerates { get { return _supportedSamplerates; } }

        private const int AUDIO_PERMISSION_REQ_ID = 1;

        private const int MAX_LOG_ENTRIES = 10;
        private readonly List<string> _logList = new List<string>();

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            UpdateSupportedSampleRates();
            AudioInputSampleRate = SupportedSamplerates[0].samplerate;
            AudioInputBufferSize = SupportedSamplerates[0].minbuffersize;

            ImageButton settingsButton = FindViewById<ImageButton>(Resource.Id.settings_open);
            settingsButton.Click += SettingsOnClick;

            ToggleButton _muteButton = FindViewById<ToggleButton>(Resource.Id.muteSwitch);
            _muteButton.Click += MuteButtonOnClick;

            ToggleButton broadcastSwitch = FindViewById<ToggleButton>(Resource.Id.broadcastSwitch);
            broadcastSwitch.Click += BroadcastOnToggle;

            _audioVisualizationLoopThread = new Thread(AudioVisualizationLoop);
            _audioVisualizationLoopThread.Start();

            _networkService = new ServerNetworkService(this);
            _networkService.OnConnectionStatusChanged += OnNetworkStatusChanged;

            LogOnScreen("Moin!");

            PermissionCheck();
        }


        private void PermissionCheck()
        {
            if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.RecordAudio) != (int)Permission.Granted)
            {
                if (ActivityCompat.ShouldShowRequestPermissionRationale(this, Manifest.Permission.RecordAudio))
                    Snackbar.Make(MainContent, Resource.String.permission_recordaudio_rationale, Snackbar.LengthIndefinite).Show();

                ActivityCompat.RequestPermissions(this, new string[] { Manifest.Permission.RecordAudio }, AUDIO_PERMISSION_REQ_ID);
            }
        }

        private void SettingsOnClick(object sender, EventArgs e)
        {
            var intent = new Intent(this, typeof(SettingsActivity));
            StartActivity(intent);
        }

        private void MuteButtonOnClick(object sender, EventArgs e)
        {
            ToggleButton btn = (ToggleButton)sender;

            _muted = btn.Checked;

            Snackbar.Make(btn, $"Microphone {(_muted == false ? "un" : "")}muted!", Snackbar.LengthShort).Show();
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            if (requestCode == AUDIO_PERMISSION_REQ_ID)
            {
                if (permissions.Length == 1)
                {
                    Snackbar.Make(MainContent, (grantResults[0] == Permission.Granted ? Resource.String.permission_recordaudio_granted : Resource.String.permission_recordaudio_denied), Snackbar.LengthShort);
                }
                else
                    Snackbar.Make(MainContent, "permission.Length != 1 :o", Snackbar.LengthShort);
            }
        }

        private void BroadcastOnToggle(object sender, EventArgs eventArgs)
        {
            View view = (View)sender;
            ToggleButton broadcastToggle = (ToggleButton)sender;

            Snackbar bar = Snackbar.Make(view, string.Empty, Snackbar.LengthShort);

            try
            {
                if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.RecordAudio) == (int)Permission.Granted)
                {
                    if (broadcastToggle.Checked)
                    {
                        _networkService.StartServer();

                        bar.SetText("Broadcast started!");

                        _recorder = new AudioRecord(AudioInputSource, AudioInputSampleRate, ChannelInputType, AudioInputEncoding, AudioInputBufferSize);
                        _recorder.StartRecording();

                        _broadcasting = true;
                    }
                    else
                    {
                        _networkService.StopServer();

                        bar.SetText("Broadcast stopped!");

                        _broadcasting = false;

                        _recorder.Stop();
                        _recorder.Release();
                    }
                }
                else
                    bar.SetText("RecordAudio Permission denied :(");
            }
            catch (Exception e)
            {
                LogOnScreen(e);
            }

            bar.Show();
        }

        private void AudioVisualizationLoop()
        {
            StringBuilder builder = new StringBuilder(); 
            int maxBars = 40;
            double barsFilled = 0;

            short[] sample;
            double maxAmplitude;
            double amplitudeDb;

            Stopwatch sw = new Stopwatch();
            int sleeptime = 50;

            while (true)
            {
                try
                {
                    Thread.Sleep(Math.Clamp(sleeptime - sw.Elapsed.Milliseconds, 0, int.MaxValue));

                    sw.Restart();

                    builder.Clear();
                    builder.Append("-80 [");

                    if (_recorder != null)
                    {
                        sample = _networkService.SampleSendData(2);

                        maxAmplitude = 0;
                        for (int i = 0; i < sample.Length; i++)
                        {
                            if (sample[i] > maxAmplitude)
                                maxAmplitude = sample[i];
                        }

                        amplitudeDb = 20 * Math.Log10((double)Math.Abs(maxAmplitude) / short.MaxValue);

                        barsFilled = ((80 + Math.Clamp(amplitudeDb, -80, 0)) / 80) * maxBars;
                    }
                    else
                        barsFilled = 0;

                    for (int i = 0; i < maxBars; i++)
                        builder.Append(i < barsFilled ? "|" : "-");

                    builder.Append("] 0db");

                    RunOnUiThread(() => { AudioVolumeView.SetText(builder.ToString(), TextView.BufferType.Normal); });

                    sw.Stop();
                }
                catch (Exception e)
                {
                    LogOnScreen(e);
                }
            }
        }

        private void OnNetworkStatusChanged(object sender, ServerNetworkService.ServerConnectionStatus status)
        {
            if (status != ServerNetworkService.ServerConnectionStatus.Shutdown)
            {
                StringBuilder connectionInfo = new StringBuilder(status.ToString());

                connectionInfo.AppendLine($"Recorder: {_recorder?.RecordingState}");
                connectionInfo.AppendLine($"Input Source: {_recorder?.AudioSource}");
                connectionInfo.AppendLine($"Samplerate: {_recorder?.SampleRate}");
                connectionInfo.AppendLine($"Buffer Size: {AudioInputBufferSize}");
                connectionInfo.AppendLine($"Channel: {_recorder?.ChannelCount}");
                connectionInfo.AppendLine($"Encoding: {_recorder?.AudioFormat}");
                
                LogOnScreen(connectionInfo.ToString());
            }

            RunOnUiThread(() => 
            { 
                BroadcasterIPLabel.Text = _networkService.EndPointString;
            });
        }


        public static void UpdateSupportedSampleRates()
        {
            List<(int, int)> validRates = new List<(int, int)>();

            foreach (int rate in new int[] { 96000, 48000, 44100, 22050, 11025, 16000, 8000 })
            {  
                int bufferSize = AudioRecord.GetMinBufferSize(rate, ChannelInputType, AudioInputEncoding);

                if (bufferSize > 0)
                    validRates.Add((rate, bufferSize));
            }

            if (validRates.Count == 0)
                validRates.Add((-1, 512));

            _supportedSamplerates = validRates.ToArray();
        }
       
        public void LogOnScreen(Exception e)
        {
            LogOnScreen($"{e.TargetSite} - {e.Source} - {e.Message}");
        }

        public void LogOnScreen(string msg)
        {
            _logList.Insert(0, $"[{DateTime.Now:HH:mm:ss}] {msg}");

            int dif = _logList.Count - MAX_LOG_ENTRIES;
            if (dif > 0)
                _logList.RemoveRange(_logList.Count-dif, dif);

            StringBuilder log = new StringBuilder();

            for (int i = 0; i < _logList.Count; i++)
                log.AppendLine(_logList[i]);

            Console.WriteLine(msg);

            RunOnUiThread(() => ConsoleOutLabel.Text = log.ToString());
        }

    }
}
