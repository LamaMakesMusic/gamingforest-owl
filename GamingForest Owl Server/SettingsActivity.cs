﻿using Android.App;
using Android.Media;
using Android.OS;
using Android.Support.V7.App;
using Android.Text;
using Android.Widget;
using System;

namespace GamingForest.Owl.Server
{
    [Activity(Label = "@string/activity_settings")]
    public class SettingsActivity : AppCompatActivity
    {
        private static int _broadcastMessagePort = 8080;
        public static int BroadcastMessagePort { get { return _broadcastMessagePort; } }
        
        private static int _broadcastAudioPort = 11000;
        public static int BroadcastAudioPort { get { return _broadcastAudioPort; } }

        private bool quiet = false;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_settings);


            ImageButton settingsClose = FindViewById<ImageButton>(Resource.Id.settings_close);
            settingsClose.Click += OnSettingsCloseClicked;


            EditText portMessageTextField = FindViewById<EditText>(Resource.Id.broadcasterMessagePortField);
            portMessageTextField.Text = _broadcastMessagePort.ToString();
            portMessageTextField.TextChanged += OnSettingsMessagePortFieldTextChanged;
            portMessageTextField.Enabled = !MainActivity.UpdateAudioLoop;

            EditText portAudioTextField = FindViewById<EditText>(Resource.Id.broadcasterAudioPortField);
            portAudioTextField.Text = _broadcastAudioPort.ToString();
            portAudioTextField.TextChanged += OnSettingsAudioPortFieldTextChanged;
            portAudioTextField.Enabled = !MainActivity.UpdateAudioLoop;

            RadioGroup inputDeviceGroup = FindViewById<RadioGroup>(Resource.Id.setting_input_source_group);
            foreach (AudioSource inputType in Enum.GetValues(typeof(AudioSource)))
            {
                if (inputType == AudioSource.Default || inputType == AudioSource.Mic || inputType == AudioSource.Unprocessed || inputType == AudioSource.Camcorder)
                {
                    inputDeviceGroup.AddView(new RadioButton(this)
                    {
                        Id = (int) inputType,
                        Text = inputType.ToString(),
                        Enabled = !MainActivity.UpdateAudioLoop
                    });
                }
            }
            inputDeviceGroup.Check((int)MainActivity.AudioInputSource);
            inputDeviceGroup.CheckedChange += OnInputDeviceChanged;

            
            RadioGroup inputSamplerateGroup = FindViewById<RadioGroup>(Resource.Id.setting_input_samplerate_group);
            inputSamplerateGroup.CheckedChange += OnInputSamplerateChanged;
            UpdateSamplerateSettings(inputSamplerateGroup);


            RadioGroup inputTypeGroup = FindViewById<RadioGroup>(Resource.Id.setting_input_type_group);
            foreach (ChannelIn inputType in Enum.GetValues(typeof(ChannelIn)))
            {
                if (inputType == ChannelIn.Default || inputType == ChannelIn.Back || inputType == ChannelIn.Front || inputType == ChannelIn.Stereo || inputType == ChannelIn.Mono)
                {
                    inputTypeGroup.AddView(new RadioButton(this)
                    {
                        Id = (int)inputType,
                        Text = inputType.ToString(),
                        Enabled = !MainActivity.UpdateAudioLoop
                    });
                }
            }
            inputTypeGroup.Check((int)MainActivity.ChannelInputType);
            inputTypeGroup.CheckedChange += OnInputChannelTypeChanged;
        }

        private void OnSettingsMessagePortFieldTextChanged(object sender, TextChangedEventArgs e)
        {
            if (quiet)
            {
                quiet = false;
                return;
            }

            if (int.TryParse(e.Text.ToString(), out int parsedPort) && parsedPort != _broadcastAudioPort)
                _broadcastMessagePort = parsedPort;
            else
            {
                quiet = true;
                EditText field = (EditText)sender;
                field.Text = _broadcastMessagePort.ToString();
            }
        }
        private void OnSettingsAudioPortFieldTextChanged(object sender, TextChangedEventArgs e)
        {
            if (quiet)
            {
                quiet = false;
                return;
            }

            if (int.TryParse(e.Text.ToString(), out int parsedPort) && parsedPort != _broadcastMessagePort)
                _broadcastAudioPort = parsedPort;
            else
            {
                quiet = true;
                EditText field = (EditText)sender;
                field.Text = _broadcastAudioPort.ToString();
            }
        }

        private void UpdateSamplerateSettings(RadioGroup inputSamplerateGroup = null)
        {
            if (inputSamplerateGroup == null)
                inputSamplerateGroup = FindViewById<RadioGroup>(Resource.Id.setting_input_samplerate_group);

            inputSamplerateGroup.RemoveAllViews();

            MainActivity.UpdateSupportedSampleRates();
            
            for (int i = 0; i < MainActivity.SupportedSamplerates.Length; i++)
            {
                inputSamplerateGroup.AddView(new RadioButton(this)
                {
                    Id = i,
                    Text = MainActivity.SupportedSamplerates[i].samplerate.ToString(),
                    Enabled = !MainActivity.UpdateAudioLoop,
                    Checked = (MainActivity.SupportedSamplerates[i].samplerate == MainActivity.AudioInputSampleRate)
                });
            }
        }

        private void OnInputChannelTypeChanged(object sender, RadioGroup.CheckedChangeEventArgs e)
        {
            MainActivity.ChannelInputType = (ChannelIn)e.CheckedId;

            UpdateSamplerateSettings();
        }

        private void OnInputSamplerateChanged(object sender, RadioGroup.CheckedChangeEventArgs e)
        {
            MainActivity.AudioInputSampleRate = MainActivity.SupportedSamplerates[e.CheckedId].samplerate;
            MainActivity.AudioInputBufferSize = MainActivity.SupportedSamplerates[e.CheckedId].minbuffersize;
        }

        private void OnInputDeviceChanged(object sender, RadioGroup.CheckedChangeEventArgs e)
        {
            MainActivity.AudioInputSource = (AudioSource)e.CheckedId;
            
            UpdateSamplerateSettings();
        }

        private void OnSettingsCloseClicked(object sender, EventArgs e)
        {
            Finish();
        }
    }
}