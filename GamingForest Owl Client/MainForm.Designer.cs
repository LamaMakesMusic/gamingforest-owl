﻿
using System;

namespace GamingForest.Owl.Client
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.serverMessageIPLabel = new System.Windows.Forms.Label();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.logoBox = new System.Windows.Forms.PictureBox();
            this.volumeBarLabel = new System.Windows.Forms.Label();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.connectionStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.playStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.outputDevicesLabel = new System.Windows.Forms.Label();
            this.outputDevicesPicker = new System.Windows.Forms.ComboBox();
            this.volumeSlider1 = new NAudio.Gui.VolumeSlider();
            this.serverAudioIPLabel = new System.Windows.Forms.Label();
            this.ipPanelGroup = new System.Windows.Forms.Panel();
            this.serverMsgIPField4 = new System.Windows.Forms.TextBox();
            this.serverMsgIPField3 = new System.Windows.Forms.TextBox();
            this.serverMsgIPField2 = new System.Windows.Forms.TextBox();
            this.serverMsgIPField1 = new System.Windows.Forms.TextBox();
            this.serverAudioIPField5 = new System.Windows.Forms.TextBox();
            this.serverMsgIPField5 = new System.Windows.Forms.TextBox();
            this.ipLabel4 = new System.Windows.Forms.Label();
            this.ipLabel3 = new System.Windows.Forms.Label();
            this.ipLabel2 = new System.Windows.Forms.Label();
            this.ipLabel1 = new System.Windows.Forms.Label();
            this.connectButton = new System.Windows.Forms.Button();
            this.logListView = new System.Windows.Forms.ListBox();
            this.ipLabel5 = new System.Windows.Forms.Label();
            this.mainPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logoBox)).BeginInit();
            this.statusStrip.SuspendLayout();
            this.ipPanelGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // serverMessageIPLabel
            // 
            this.serverMessageIPLabel.AutoSize = true;
            this.serverMessageIPLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serverMessageIPLabel.Location = new System.Drawing.Point(10, 230);
            this.serverMessageIPLabel.Name = "serverMessageIPLabel";
            this.serverMessageIPLabel.Size = new System.Drawing.Size(193, 18);
            this.serverMessageIPLabel.TabIndex = 0;
            this.serverMessageIPLabel.Text = "Server IP and Message Port";
            this.serverMessageIPLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mainPanel
            // 
            this.mainPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mainPanel.Controls.Add(this.logoBox);
            this.mainPanel.Controls.Add(this.volumeBarLabel);
            this.mainPanel.Controls.Add(this.statusStrip);
            this.mainPanel.Controls.Add(this.outputDevicesLabel);
            this.mainPanel.Controls.Add(this.outputDevicesPicker);
            this.mainPanel.Controls.Add(this.volumeSlider1);
            this.mainPanel.Controls.Add(this.serverAudioIPLabel);
            this.mainPanel.Controls.Add(this.serverMessageIPLabel);
            this.mainPanel.Controls.Add(this.ipPanelGroup);
            this.mainPanel.Controls.Add(this.connectButton);
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.mainPanel.Location = new System.Drawing.Point(0, 0);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(444, 521);
            this.mainPanel.TabIndex = 19;
            // 
            // logoBox
            // 
            this.logoBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.logoBox.Image = global::GamingForest.Owl.Client.Properties.Resources.GFLogo;
            this.logoBox.Location = new System.Drawing.Point(0, 0);
            this.logoBox.Name = "logoBox";
            this.logoBox.Size = new System.Drawing.Size(442, 200);
            this.logoBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.logoBox.TabIndex = 5;
            this.logoBox.TabStop = false;
            // 
            // volumeBarLabel
            // 
            this.volumeBarLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.volumeBarLabel.CausesValidation = false;
            this.volumeBarLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.volumeBarLabel.Location = new System.Drawing.Point(-4, 400);
            this.volumeBarLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.volumeBarLabel.Name = "volumeBarLabel";
            this.volumeBarLabel.Size = new System.Drawing.Size(435, 20);
            this.volumeBarLabel.TabIndex = 17;
            this.volumeBarLabel.Text = "-80[||||||||||||||||||||------------------------------]0db";
            this.volumeBarLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.connectionStatusLabel,
            this.playStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 497);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(442, 22);
            this.statusStrip.SizingGrip = false;
            this.statusStrip.TabIndex = 4;
            this.statusStrip.Text = "status";
            // 
            // connectionStatusLabel
            // 
            this.connectionStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.connectionStatusLabel.Name = "connectionStatusLabel";
            this.connectionStatusLabel.Size = new System.Drawing.Size(101, 17);
            this.connectionStatusLabel.Text = "ConnectionStatus";
            this.connectionStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // playStatusLabel
            // 
            this.playStatusLabel.Name = "playStatusLabel";
            this.playStatusLabel.Size = new System.Drawing.Size(61, 17);
            this.playStatusLabel.Text = "PlayStatus";
            // 
            // outputDevicesLabel
            // 
            this.outputDevicesLabel.AutoSize = true;
            this.outputDevicesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.outputDevicesLabel.Location = new System.Drawing.Point(10, 310);
            this.outputDevicesLabel.Name = "outputDevicesLabel";
            this.outputDevicesLabel.Size = new System.Drawing.Size(101, 18);
            this.outputDevicesLabel.TabIndex = 16;
            this.outputDevicesLabel.Text = "Output Device";
            this.outputDevicesLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // outputDevicesPicker
            // 
            this.outputDevicesPicker.FormattingEnabled = true;
            this.outputDevicesPicker.Location = new System.Drawing.Point(15, 331);
            this.outputDevicesPicker.Name = "outputDevicesPicker";
            this.outputDevicesPicker.Size = new System.Drawing.Size(403, 21);
            this.outputDevicesPicker.TabIndex = 15;
            this.outputDevicesPicker.SelectedIndexChanged += new System.EventHandler(this.outputDevicesPicker_SelectedIndexChanged);
            // 
            // volumeSlider1
            // 
            this.volumeSlider1.Location = new System.Drawing.Point(15, 370);
            this.volumeSlider1.Name = "volumeSlider1";
            this.volumeSlider1.Size = new System.Drawing.Size(403, 27);
            this.volumeSlider1.TabIndex = 14;
            this.volumeSlider1.VolumeChanged += new System.EventHandler(this.volumeSlider1_VolumeChanged);
            // 
            // serverAudioIPLabel
            // 
            this.serverAudioIPLabel.AutoSize = true;
            this.serverAudioIPLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serverAudioIPLabel.Location = new System.Drawing.Point(305, 230);
            this.serverAudioIPLabel.Name = "serverAudioIPLabel";
            this.serverAudioIPLabel.Size = new System.Drawing.Size(124, 18);
            this.serverAudioIPLabel.TabIndex = 12;
            this.serverAudioIPLabel.Text = "Server Audio Port";
            this.serverAudioIPLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ipPanelGroup
            // 
            this.ipPanelGroup.BackColor = System.Drawing.SystemColors.Control;
            this.ipPanelGroup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ipPanelGroup.Controls.Add(this.ipLabel5);
            this.ipPanelGroup.Controls.Add(this.serverMsgIPField4);
            this.ipPanelGroup.Controls.Add(this.serverMsgIPField3);
            this.ipPanelGroup.Controls.Add(this.serverMsgIPField2);
            this.ipPanelGroup.Controls.Add(this.serverMsgIPField1);
            this.ipPanelGroup.Controls.Add(this.serverAudioIPField5);
            this.ipPanelGroup.Controls.Add(this.serverMsgIPField5);
            this.ipPanelGroup.Controls.Add(this.ipLabel4);
            this.ipPanelGroup.Controls.Add(this.ipLabel3);
            this.ipPanelGroup.Controls.Add(this.ipLabel2);
            this.ipPanelGroup.Controls.Add(this.ipLabel1);
            this.ipPanelGroup.Location = new System.Drawing.Point(12, 250);
            this.ipPanelGroup.Margin = new System.Windows.Forms.Padding(0);
            this.ipPanelGroup.Name = "ipPanelGroup";
            this.ipPanelGroup.Size = new System.Drawing.Size(410, 36);
            this.ipPanelGroup.TabIndex = 10;
            // 
            // serverMsgIPField4
            // 
            this.serverMsgIPField4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serverMsgIPField4.Location = new System.Drawing.Point(172, 3);
            this.serverMsgIPField4.MaxLength = 3;
            this.serverMsgIPField4.Name = "serverMsgIPField4";
            this.serverMsgIPField4.Size = new System.Drawing.Size(37, 29);
            this.serverMsgIPField4.TabIndex = 8;
            this.serverMsgIPField4.Text = "255";
            this.serverMsgIPField4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.serverMsgIPField4.Validated += new System.EventHandler(this.serverMessageIPField4_TextChanged);
            // 
            // serverMsgIPField3
            // 
            this.serverMsgIPField3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serverMsgIPField3.Location = new System.Drawing.Point(120, 3);
            this.serverMsgIPField3.MaxLength = 3;
            this.serverMsgIPField3.Name = "serverMsgIPField3";
            this.serverMsgIPField3.Size = new System.Drawing.Size(37, 29);
            this.serverMsgIPField3.TabIndex = 7;
            this.serverMsgIPField3.Text = "255";
            this.serverMsgIPField3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.serverMsgIPField3.Validated += new System.EventHandler(this.serverMessageIPField3_TextChanged);
            // 
            // serverMsgIPField2
            // 
            this.serverMsgIPField2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serverMsgIPField2.Location = new System.Drawing.Point(66, 3);
            this.serverMsgIPField2.MaxLength = 3;
            this.serverMsgIPField2.Name = "serverMsgIPField2";
            this.serverMsgIPField2.Size = new System.Drawing.Size(37, 29);
            this.serverMsgIPField2.TabIndex = 6;
            this.serverMsgIPField2.Text = "255";
            this.serverMsgIPField2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.serverMsgIPField2.Validated += new System.EventHandler(this.serverMessageIPField2_TextChanged);
            // 
            // serverMsgIPField1
            // 
            this.serverMsgIPField1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serverMsgIPField1.Location = new System.Drawing.Point(12, 3);
            this.serverMsgIPField1.MaxLength = 3;
            this.serverMsgIPField1.Name = "serverMsgIPField1";
            this.serverMsgIPField1.Size = new System.Drawing.Size(37, 29);
            this.serverMsgIPField1.TabIndex = 1;
            this.serverMsgIPField1.Text = "255";
            this.serverMsgIPField1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.serverMsgIPField1.Validated += new System.EventHandler(this.serverMessageIPField1_TextChanged);
            // 
            // serverAudioIPField5
            // 
            this.serverAudioIPField5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serverAudioIPField5.Location = new System.Drawing.Point(310, 3);
            this.serverAudioIPField5.MaxLength = 5;
            this.serverAudioIPField5.Name = "serverAudioIPField5";
            this.serverAudioIPField5.Size = new System.Drawing.Size(68, 29);
            this.serverAudioIPField5.TabIndex = 9;
            this.serverAudioIPField5.Text = "65555";
            this.serverAudioIPField5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.serverAudioIPField5.Validated += new System.EventHandler(this.serverAudioIPField5_TextChanged);
            // 
            // serverMsgIPField5
            // 
            this.serverMsgIPField5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serverMsgIPField5.Location = new System.Drawing.Point(223, 3);
            this.serverMsgIPField5.MaxLength = 5;
            this.serverMsgIPField5.Name = "serverMsgIPField5";
            this.serverMsgIPField5.Size = new System.Drawing.Size(68, 29);
            this.serverMsgIPField5.TabIndex = 9;
            this.serverMsgIPField5.Text = "65555";
            this.serverMsgIPField5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.serverMsgIPField5.Validated += new System.EventHandler(this.serverMessageIPField5_TextChanged);
            // 
            // ipLabel4
            // 
            this.ipLabel4.AutoSize = true;
            this.ipLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ipLabel4.Location = new System.Drawing.Point(208, 7);
            this.ipLabel4.Margin = new System.Windows.Forms.Padding(0);
            this.ipLabel4.Name = "ipLabel4";
            this.ipLabel4.Size = new System.Drawing.Size(16, 22);
            this.ipLabel4.TabIndex = 13;
            this.ipLabel4.Text = ":";
            this.ipLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ipLabel3
            // 
            this.ipLabel3.AutoSize = true;
            this.ipLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ipLabel3.Location = new System.Drawing.Point(157, 8);
            this.ipLabel3.Margin = new System.Windows.Forms.Padding(0);
            this.ipLabel3.Name = "ipLabel3";
            this.ipLabel3.Size = new System.Drawing.Size(16, 22);
            this.ipLabel3.TabIndex = 12;
            this.ipLabel3.Text = ".";
            this.ipLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ipLabel2
            // 
            this.ipLabel2.AutoSize = true;
            this.ipLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ipLabel2.Location = new System.Drawing.Point(104, 8);
            this.ipLabel2.Margin = new System.Windows.Forms.Padding(0);
            this.ipLabel2.Name = "ipLabel2";
            this.ipLabel2.Size = new System.Drawing.Size(16, 22);
            this.ipLabel2.TabIndex = 11;
            this.ipLabel2.Text = ".";
            this.ipLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ipLabel1
            // 
            this.ipLabel1.AutoSize = true;
            this.ipLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ipLabel1.Location = new System.Drawing.Point(50, 8);
            this.ipLabel1.Margin = new System.Windows.Forms.Padding(0);
            this.ipLabel1.Name = "ipLabel1";
            this.ipLabel1.Size = new System.Drawing.Size(16, 22);
            this.ipLabel1.TabIndex = 10;
            this.ipLabel1.Text = ".";
            this.ipLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // connectButton
            // 
            this.connectButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.connectButton.Location = new System.Drawing.Point(89, 446);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(240, 40);
            this.connectButton.TabIndex = 2;
            this.connectButton.Text = "Connect";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // logListView
            // 
            this.logListView.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.logListView.FormattingEnabled = true;
            this.logListView.HorizontalScrollbar = true;
            this.logListView.Items.AddRange(new object[] {
            "kasjödfasjdfölkasjdf askdfj asödfj laksdfj ölasdjf lökasjdf ölasdfj",
            "akldsfj ölaksjdfp oij qlkm admf,asmdölasdjf askjdf ",
            "ajsdfklajsdfölkjajasfd",
            "as",
            "df",
            "asdf",
            "as",
            "df",
            "asdkfjlasjdfölasjdfökjas d",
            "f",
            "asd fasdf asfdkasjd flkasj fa",
            "sdfjas kdfj asödf"});
            this.logListView.Location = new System.Drawing.Point(450, 0);
            this.logListView.Name = "logListView";
            this.logListView.Size = new System.Drawing.Size(434, 520);
            this.logListView.TabIndex = 18;
            // 
            // ipLabel5
            // 
            this.ipLabel5.AutoSize = true;
            this.ipLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ipLabel5.Location = new System.Drawing.Point(293, 7);
            this.ipLabel5.Margin = new System.Windows.Forms.Padding(0);
            this.ipLabel5.Name = "ipLabel5";
            this.ipLabel5.Size = new System.Drawing.Size(16, 22);
            this.ipLabel5.TabIndex = 14;
            this.ipLabel5.Text = "/";
            this.ipLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 521);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.logListView);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(900, 560);
            this.MinimumSize = new System.Drawing.Size(460, 560);
            this.Name = "MainForm";
            this.Text = "GamingForest Owl Client";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.mainPanel.ResumeLayout(false);
            this.mainPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logoBox)).EndInit();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ipPanelGroup.ResumeLayout(false);
            this.ipPanelGroup.PerformLayout();
            this.ResumeLayout(false);

        }


        #endregion

        private System.Windows.Forms.Label serverMessageIPLabel;
        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel connectionStatusLabel;
        private System.Windows.Forms.PictureBox logoBox;
        private System.Windows.Forms.Label serverAudioIPLabel;
        private NAudio.Gui.VolumeSlider volumeSlider1;
        private System.Windows.Forms.ComboBox outputDevicesPicker;
        private System.Windows.Forms.Label outputDevicesLabel;
        private System.Windows.Forms.ToolStripStatusLabel playStatusLabel;
        private System.Windows.Forms.Label volumeBarLabel;
        private System.Windows.Forms.ListBox logListView;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.Label ipLabel1;
        private System.Windows.Forms.Label ipLabel2;
        private System.Windows.Forms.Label ipLabel3;
        private System.Windows.Forms.Label ipLabel4;
        private System.Windows.Forms.TextBox serverMsgIPField5;
        private System.Windows.Forms.TextBox serverAudioIPField5;
        private System.Windows.Forms.TextBox serverMsgIPField1;
        private System.Windows.Forms.TextBox serverMsgIPField2;
        private System.Windows.Forms.TextBox serverMsgIPField3;
        private System.Windows.Forms.TextBox serverMsgIPField4;
        private System.Windows.Forms.Panel ipPanelGroup;
        private System.Windows.Forms.Label ipLabel5;
    }
}

