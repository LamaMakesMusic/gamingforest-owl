﻿using NAudio.Wave;
using System;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace GamingForest.Owl.Client
{
    static class Program
    {
        public static ClientNetworkService Network;

        private static MainForm _form;
        private static Thread _audioVisualizationLoop;

        public static readonly WaveOut Player = new WaveOut();

        private static BufferedWaveProvider _bufferedProvider;
        public static BufferedWaveProvider BufferedProvider { get { return _bufferedProvider; } }

        public static int SampleRate;
        public static int BitDepth;
        public static int Channels;


        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            _form = new MainForm();

            Network = new ClientNetworkService(_form);
            Network.OnConnectionStatusChanged += _form.OnClientConnectionStatusChanged;

            _audioVisualizationLoop = new Thread(AudioVisualizationLoop);

            _audioVisualizationLoop.Start();


            Application.Run(_form);

            _audioVisualizationLoop.Abort();
        }

        private static void AudioVisualizationLoop()
        {
            int maxBars = 50;
            double barsFilled;

            short[] sample;
            double maxAmplitude;
            double amplitudeDb;

            StringBuilder builder = new StringBuilder();

            Stopwatch sw = new Stopwatch();
            int sleeptime = 50;

            while (true)
            {
                try
                {
                    Thread.Sleep(Clamp(sleeptime - sw.Elapsed.Milliseconds, 0, int.MaxValue));

                    sw.Restart();

                    builder.Clear();
                    builder.Append("-80 [");

                    if (Player.PlaybackState == PlaybackState.Playing)
                    {
                        sample = Network.SampleReceiveData(2);
                        
                        maxAmplitude = 0;
                        for (int i = 0; i < sample.Length; i++)
                        {
                            if (sample[i] > maxAmplitude)
                                maxAmplitude = sample[i];
                        }

                        amplitudeDb = 20 * Math.Log10((double)Math.Abs(maxAmplitude) / short.MaxValue);

                        barsFilled = ((80 + Clamp(amplitudeDb, -80, 0)) / 80) * maxBars;
                    }
                    else
                        barsFilled = 0;

                    for (int i = 0; i < maxBars; i++)
                        builder.Append(i < barsFilled ? "|" : "-");

                    builder.Append("] 0db");

                    _form.UpdateVolumeLabel(builder.ToString());
                    _form.UpdatePlayStatus(Player.PlaybackState.ToString());

                    sw.Stop();
                }
                catch (Exception e)
                {
                    _form.LogOnScreen(e);
                }

            }

        }

        public static void RefreshPlayer()
        {
            if (Player.PlaybackState != PlaybackState.Stopped)
                Player.Stop();

            for (int i = 0; i < WaveOut.DeviceCount; i++)
            {
                var caps = WaveOut.GetCapabilities(i);
                _form.LogOnScreen($"{i} - {caps.ProductName}");
            }

            _bufferedProvider = new BufferedWaveProvider(new WaveFormat(SampleRate, BitDepth, Channels));
            _bufferedProvider.DiscardOnBufferOverflow = true;
            _bufferedProvider.ReadFully = true;
            _bufferedProvider.BufferDuration = TimeSpan.FromMilliseconds(400);

            Player.DeviceNumber = _form.OutputDeviceID;
            Player.Volume = Properties.Settings.Default.volumeSliderValue;

            Player.Init(_bufferedProvider);
            Player.Play();
        }

        public static double Clamp(double value, double min, double max)
        {
            if (value > max)
                return max;

            if (value < min)
                return min;

            return value;
        }
        public static int Clamp(int value, int min, int max)
        {
            if (value > max)
                return max;

            if (value < min)
                return min;

            return value;
        }
    }
}
