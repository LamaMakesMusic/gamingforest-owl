﻿using NAudio.Gui;
using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace GamingForest.Owl.Client
{
    public partial class MainForm : Form
    {
        public int OutputDeviceID = 0;
        
        private List<string> _logList = new List<string>();
        private const int MAX_LOG_ENTRIES = 100;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            string[] ip = Properties.Settings.Default.serverIP.Split(new char[] { '.', ':' });

            this.serverMsgIPField1.Text = ip[0];
            this.serverMsgIPField2.Text = ip[1];
            this.serverMsgIPField3.Text = ip[2];
            this.serverMsgIPField4.Text = ip[3];
            this.serverMsgIPField5.Text = ip[4];
            this.serverAudioIPField5.Text = ip[5];

            this.volumeSlider1.Volume = Properties.Settings.Default.volumeSliderValue;

            for (int i = 0; i < WaveOut.DeviceCount; i++)
            {
                WaveOutCapabilities cap = WaveOut.GetCapabilities(i);
                
                this.outputDevicesPicker.Items.Add(cap.ProductName);

                if (cap.ProductName == Properties.Settings.Default.outputDeviceName)
                    this.outputDevicesPicker.SelectedIndex = i;
            }

            OnClientConnectionStatusChanged(null, ClientNetworkService.ConnectionStatus.Offline);

            LogOnScreen("Moin!");
        }

        Action buttonCallback;
        internal void OnClientConnectionStatusChanged(object sender, ClientNetworkService.ConnectionStatus e)
        {

            string buttonText = e.ToString();
            bool connectButtonEnabled = (e != ClientNetworkService.ConnectionStatus.Disconnecting);
            bool inputFieldsEnabled = (e == ClientNetworkService.ConnectionStatus.Offline || e == ClientNetworkService.ConnectionStatus.Error);

            switch (e)
            {
                case ClientNetworkService.ConnectionStatus.Offline:
                case ClientNetworkService.ConnectionStatus.Disconnecting:
                case ClientNetworkService.ConnectionStatus.Error:
                    buttonText = "Connect";
                    buttonCallback = () => Program.Network.StartClient();
                    break;                
                
                case ClientNetworkService.ConnectionStatus.Connecting:
                case ClientNetworkService.ConnectionStatus.Connected:
                    buttonText = "Disconnect";
                    buttonCallback = () => Program.Network.StopClient();
                    break;
                
                default:
                    break;
            }

            this.BeginInvoke(new MethodInvoker(() => 
            {
                this.connectButton.Text = buttonText;
                this.connectButton.Enabled = connectButtonEnabled;

                this.connectionStatusLabel.Text = e.ToString();

                this.serverMsgIPField1.Enabled = inputFieldsEnabled;
                this.serverMsgIPField2.Enabled = inputFieldsEnabled;
                this.serverMsgIPField3.Enabled = inputFieldsEnabled;
                this.serverMsgIPField4.Enabled = inputFieldsEnabled;
                this.serverMsgIPField5.Enabled = inputFieldsEnabled;
                this.serverAudioIPField5.Enabled = inputFieldsEnabled;

                this.outputDevicesPicker.Enabled = inputFieldsEnabled;
            }));
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            buttonCallback?.Invoke();
        }

        private void serverMessageIPField1_TextChanged(object sender, EventArgs e)
        {
            validateIPEntry((TextBox)sender, 0);
        }
        private void serverMessageIPField2_TextChanged(object sender, EventArgs e)
        {
            validateIPEntry((TextBox)sender, 1);
        }
        private void serverMessageIPField3_TextChanged(object sender, EventArgs e)
        {
            validateIPEntry((TextBox)sender, 2);
        }
        private void serverMessageIPField4_TextChanged(object sender, EventArgs e)
        {
            validateIPEntry((TextBox)sender, 3);
        }
        private void serverMessageIPField5_TextChanged(object sender, EventArgs e)
        {
            validateIPEntry((TextBox)sender, 4);
        }

        private void serverAudioIPField5_TextChanged(object sender, EventArgs e)
        {
            validateIPEntry((TextBox)sender, 5);
        }

        private void validateIPEntry(TextBox textBox, int entryIndex)
        {
            string[] splitIp = Properties.Settings.Default.serverIP.Split(new char[] { '.', ':' });

            if (int.TryParse(textBox.Text, out int parsed))
            {
                if (parsed < 0)
                    parsed = 0;
                else if (entryIndex < 4)
                    parsed = parsed > 255 ? 255 : parsed;
                else if (entryIndex > 3)
                {
                    if (parsed > int.MaxValue)
                        parsed = int.MaxValue;

                    if (entryIndex == 4 && parsed == int.Parse(splitIp[5]))
                        parsed = int.Parse(splitIp[4]);
                    else if (entryIndex == 5 && parsed == int.Parse(splitIp[4]))
                        parsed = int.Parse(splitIp[5]);
                }
                
                splitIp[entryIndex] = parsed.ToString();

                Properties.Settings.Default.serverIP = $"{splitIp[0]}.{splitIp[1]}.{splitIp[2]}.{splitIp[3]}:{splitIp[4]}:{splitIp[5]}";

                Properties.Settings.Default.Save();
            }
            else
            {
                LogOnScreen($"Could not parse entry '{textBox.Text}'!");
            }

            this.serverMsgIPField1.Text = splitIp[0];
            this.serverMsgIPField2.Text = splitIp[1];
            this.serverMsgIPField3.Text = splitIp[2];
            this.serverMsgIPField4.Text = splitIp[3];
            this.serverMsgIPField5.Text = splitIp[4];
            this.serverAudioIPField5.Text = splitIp[5];
        }

        private void volumeSlider1_VolumeChanged(object sender, EventArgs e)
        {
            VolumeSlider volSlider = (VolumeSlider)sender;

            Program.Player.Volume = volSlider.Volume;
            
            Properties.Settings.Default.volumeSliderValue = volumeSlider1.Volume;
            Properties.Settings.Default.Save();
        }

        public void UpdateVolumeLabel(string text)
        {
            this.BeginInvoke(new MethodInvoker(() => 
            {
                this.volumeBarLabel.Text = text;
            }));
        }
        public void UpdatePlayStatus(string status)
        {
            this.BeginInvoke(new MethodInvoker(() =>
            {
                this.playStatusLabel.Text = status;
            }));
        }

        private void outputDevicesPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var cBox = (ComboBox)sender;

            OutputDeviceID = cBox.SelectedIndex;

            Properties.Settings.Default.outputDeviceName = (string) cBox.SelectedItem;
            Properties.Settings.Default.Save();
        }
    
        public void LogOnScreen(Exception e)
        {
            LogOnScreen($"{e.TargetSite} - {e.Source} - {e.Message}");
        }
        public void LogOnScreen(string msg)
        {
            _logList.Insert(0, $"[{DateTime.Now:HH:mm:ss}] {msg}");

            int dif = _logList.Count - MAX_LOG_ENTRIES;
            if (dif > 0)
                _logList.RemoveRange(_logList.Count - dif, dif);

            Console.WriteLine(msg);

            if (logListView.IsHandleCreated)
            {
                logListView.BeginInvoke(new MethodInvoker(() => 
                {
                    logListView.Items.Clear();
                    for (int i = 0; i < _logList.Count; i++)
                        logListView.Items.Add(_logList[i]);
                }));
            }
        }
    }
}
