﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace GamingForest.Owl.Client
{
    public class ClientNetworkService
    {
        public ConnectionStatus Status
        {
            get { return _status; }
            set
            {
                _status = value;
                OnConnectionStatusChanged.Invoke(this, value);
            }
        }
        private ConnectionStatus _status;

        public EventHandler<ConnectionStatus> OnConnectionStatusChanged;

        public enum ConnectionStatus
        {
            Offline = 0,
            Connecting = 1,
            Connected = 2,
            Disconnecting = 3,
            Error = 4
        }

        public bool Running { get { return _running; } }
        private bool _running = false;

        private const int MSG_BUFFER_SIZE = 1024;
        private int _audioReceiveBufferSize = 1024;

        private byte[] _audioReceiveBuffer;

        private Thread _clientThread;

        private Socket _messageSender;
        private Socket _audioSender;

        private bool _handshake;
        private MainForm _form;


        public ClientNetworkService(MainForm mainForm)
        {
            _form = mainForm;
        }

        public void StartClient()
        {
            if (_clientThread != null)
            {
                StopClient();
                _clientThread?.Join();
            }

            if (_clientThread != null)
                throw new Exception("Could not stop existing client thread!");

            _clientThread = new Thread(ClientRoutine)
            {
                Name = nameof(ClientRoutine)
            };
            _clientThread.Start();
        }

        private void ClientRoutine()
        {
            try
            {
                _handshake = false;
                _running = true;

                string[] ipSplit = Properties.Settings.Default.serverIP.Split(new char[] { ':' });
                IPAddress serverIp = IPAddress.Parse(ipSplit[0]);
                int msgPort = int.Parse(ipSplit[1]);
                int audioPort = int.Parse(ipSplit[2]);

                IPEndPoint msgEndPoint = new IPEndPoint(serverIp, msgPort);
                IPEndPoint audioEndPoint = new IPEndPoint(serverIp, audioPort);

                _messageSender = new Socket(serverIp.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                _audioSender = new Socket(serverIp.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                Thread receiveMessageThread = new Thread(ReceiveMessageThread)
                {
                    Name = nameof(ReceiveMessageThread)
                };
                Thread receiveAudioThread = new Thread(ReceiveAudioThread)
                {
                    Name = nameof(ReceiveAudioThread)
                };

                Status = ConnectionStatus.Connecting;

                _messageSender.Connect(msgEndPoint);
                _form.LogOnScreen($"[MESSAGE] Connected to {_messageSender.RemoteEndPoint}...");

                _audioSender.Connect(audioEndPoint);
                _form.LogOnScreen($"[AUDIO] Connected to {_audioSender.RemoteEndPoint}...");

                receiveMessageThread.Start();

                _form.LogOnScreen("Waiting for handshake...");

                while (!_handshake)
                    Thread.Sleep(100);

                _form.LogOnScreen("Handshake completed...");
                
                receiveAudioThread.Start();

                Status = ConnectionStatus.Connected;

                while (_running)
                    Thread.Sleep(100);

                _form.LogOnScreen("Disconnecting from Server...");

                Status = ConnectionStatus.Disconnecting;
            }
            catch (Exception e)
            {
                if (e is SocketException se)
                    _form.LogOnScreen($"{se.SocketErrorCode} | {se.Message}");
                else
                    _form.LogOnScreen(e);

                Status = ConnectionStatus.Error;
            }
            finally
            {
                _running = false;

                TryDisposeSenders();

                Status = ConnectionStatus.Offline;

                _clientThread = null;
            }
        }

        public void StopClient()
        {
            SendMessageToServer("<R2D> Client shutting down...");

            _running = false;

            Thread.Sleep(500);

            TryDisposeSenders();
        }


        private void TryDisposeSenders()
        {
            try
            {
                if (_messageSender != null)
                {
                    _messageSender.Close();
                    _messageSender.Dispose();
                }

                if (_audioSender != null)
                {
                    _audioSender.Close();
                    _audioSender.Dispose();
                }
            }
            catch (Exception e)
            {
                _form.LogOnScreen(e);
            }
        }


        private void ReceiveMessageThread()
        {
            byte[] receiveBuffer = new byte[MSG_BUFFER_SIZE];
            string msgReceived;
            int byteCount;
            _messageSender.ReceiveTimeout = 8000;

            while (_running)
            {
                try
                {
                    if (_messageSender == null || _messageSender.Connected == false)
                        continue;

                    if (!_handshake)
                        SendMessageToServer("<R2C> Say Hi!");

                    byteCount = _messageSender.Receive(receiveBuffer);
                    msgReceived = Encoding.ASCII.GetString(receiveBuffer, 0, byteCount);

                    _form.LogOnScreen($"IN from {_messageSender.RemoteEndPoint}: {msgReceived}");

                    if (msgReceived.StartsWith("<R2D>"))
                        _running = false;
                    else if (msgReceived.StartsWith("<R2C>"))
                    {
                        foreach (string line in msgReceived.Split('\n'))
                        {
                            if (line.StartsWith("<BUFFER>"))
                            {
                                if (int.TryParse(line.Substring(8), out int parsedInputBufferSize))
                                {
                                    _audioReceiveBufferSize = parsedInputBufferSize;
                                    _form.LogOnScreen($"Audio Input Buffer Size set to '{parsedInputBufferSize}'");
                                }
                                else
                                    _form.LogOnScreen($"Could not parse Buffer Size Argument: '{line}'");
                            }
                            else if (line.StartsWith("<RATE>"))
                            {
                                if (int.TryParse(line.Substring(6), out int parsedSampleRate))
                                {
                                    Program.SampleRate = parsedSampleRate;
                                    _form.LogOnScreen($"Audio Buffer Size set to '{parsedSampleRate}'");
                                }
                                else
                                    _form.LogOnScreen($"Could not parse Audio Sample Rate Argument: '{line}'");
                            }
                            else if (line.StartsWith("<CHANNELS>"))
                            {
                                if (int.TryParse(line.Substring(10), out int parsedChannelCount))
                                {
                                    Program.Channels = parsedChannelCount;
                                    _form.LogOnScreen($"Audio Channel Count set to '{parsedChannelCount}'");
                                }
                                else
                                    _form.LogOnScreen($"Could not parse Audio Channels Argument: '{line}'");
                            }
                            else if (line.StartsWith("<ENCODING>"))
                            {
                                if (line.Substring(10) is string sub)
                                {
                                    if (sub == "Pcm8bit")
                                        Program.BitDepth = 8;
                                    else if (sub == "Pcm16bit")
                                        Program.BitDepth = 16;
                                    else
                                        Program.BitDepth = 24;

                                    _form.LogOnScreen($"Audio Bit Depth set to '{Program.BitDepth}'");
                                }
                                else
                                    _form.LogOnScreen($"Could not parse Encoding Argument: '{line}'");
                            }
                        }

                        _handshake = true;
                        _messageSender.ReceiveTimeout = 0;
                    }

                }
                catch (Exception e)
                {
                    if (e is SocketException se)
                    {
                        if (!_handshake && se.SocketErrorCode == SocketError.TimedOut)
                            _form.LogOnScreen("Still waiting for handshake...");
                        else
                            _form.LogOnScreen($"{se.SocketErrorCode} | {se.Message}");
                    }
                    else
                        _form.LogOnScreen(e);
                }
            }
        }

        private void ReceiveAudioThread()
        {
            Program.RefreshPlayer();

            _audioReceiveBuffer = new byte[_audioReceiveBufferSize];
            int byteCount;

            while (_running)
            {
                try
                {
                    if (_audioSender == null || _audioSender.Connected == false)
                        continue;

                    byteCount = _audioSender.Receive(_audioReceiveBuffer);

                    Program.BufferedProvider.AddSamples(_audioReceiveBuffer, 0, byteCount);
                }
                catch (Exception e)
                {
                    if (e is SocketException se)
                        _form.LogOnScreen($"{se.SocketErrorCode} | {se.Message}");
                    else
                        _form.LogOnScreen(e);
                }
            }

            Program.BufferedProvider.ClearBuffer();
            Program.Player.Stop();
        }

        public short[] SampleReceiveData(int sampleSize)
        {
            short[] sample = new short[sampleSize];

            if (_audioReceiveBuffer != null)
                Buffer.BlockCopy(_audioReceiveBuffer, 0, sample, 0, sampleSize);

            return sample;
        }

        private void SendMessageToServer(string msg)
        {
            try
            {
                if (_messageSender == null || _messageSender.Connected == false)
                    return;

                _form.LogOnScreen($"OUT to {_messageSender.RemoteEndPoint}: {msg}");

                _messageSender.Send(Encoding.ASCII.GetBytes(msg));
            }
            catch (Exception e)
            {
                _form.LogOnScreen(e);
            }
        }

    }
}
